package u04lab.code

import scala.annotation.tailrec
import scala.language.postfixOps // silence warnings

sealed trait List[A] {

  def head: Option[A]

  def tail: Option[List[A]]

  def append(list: List[A]): List[A]

  def foreach(consumer: (A) => Unit): Unit

  def get(pos: Int): Option[A]

  def filter(predicate: (A) => Boolean): List[A]

  def map[B](fun: (A) => B): List[B]

  def toSeq: Seq[A]

  def flatMap[B](f: A => List[B]): List[B]

  def zipRight: List[(A, Int)]

  // right-associative construction: 10 :: 20 :: 30 :: Nil()
  def ::(head: A): List[A] = Cons(head, this)

  def contains(head: A): Boolean

  def exists(f: A => Boolean): Boolean

  def forAll(f: A => Boolean): Boolean

  def forNone(f: A => Boolean): Boolean

}

// defining concrete implementations based on the same template

case class Cons[A](_head: A, _tail: List[A])
  extends ListImplementation[A]

case class Nil[A]()
  extends ListImplementation[A]

// enabling pattern matching on ::

object :: {
  def unapply[A](l: List[A]): Option[(A, List[A])] = l match {
    case Cons(h, t) => Some((h, t))
    case _ => None
  }
}

// List algorithms
trait ListImplementation[A] extends List[A] {

  override def head: Option[A] = this match {
    case h :: _ => Some(h)
    case _ => None
  }

  override def tail: Option[List[A]] = this match {
    case _ :: t => Some(t)
    case _ => None
  }

  override def append(list: List[A]): List[A] = this match {
    case h :: t => h :: (t append list)
    case _ => list
  }

  override def foreach(consumer: A => Unit): Unit = this match {
    case h :: t => consumer(h); t foreach consumer
    case _ => None
  }

  override def get(pos: Int): Option[A] = this match {
    case h :: t if pos == 0 => Some(h)
    case h :: t if pos > 0 => t get (pos - 1)
    case _ => None
  }

  override def filter(predicate: A => Boolean): List[A] = this match {
    case h :: t if predicate(h) => h :: (t filter predicate)
    case _ :: t => t filter predicate
    case _ => Nil()
  }

  override def map[B](fun: A => B): List[B] = this match {
    case h :: t => fun(h) :: (t map fun)
    case _ => Nil()
  }

  override def toSeq: Seq[A] = this match {
    case h :: t => h +: t.toSeq // using method '+:' in Seq..
    case _ => Seq()
  }

  override def zipRight: List[(A, Int)] = {
    def _zipRight(l: List[A], i: Int): List[(A, Int)] = l match {
      case h :: t => (h, i) :: _zipRight(t, i + 1)
      case _ => Nil()
    }

    _zipRight(this, 0)
  }

  override def flatMap[B](f: A => List[B]): List[B] = this match {
    case h :: t => f(h) append (t flatMap f)
    case _ => Nil()
  }

  override def contains(h: A): Boolean = exists {
    _ == h
  }

  override def exists(f: A => Boolean): Boolean = {
    @tailrec
    def _exists(l: List[A]): Boolean = l match {
      case h :: _ if f(h) => true
      case h :: t if !f(h) => _exists(t)
      case _ => false
    }

    _exists(this)
  }

  override def forAll(f: A => Boolean): Boolean = {
    @tailrec
    def _forAll(l: List[A]): Boolean = l match {
      case h :: t if f(h) => _forAll(t)
      case h :: _ if !f(h) => false
      case _ => true
    }

    this match {
      case h :: t => _forAll(this)
      case _ => false
    }
  }

  override def forNone(f: A => Boolean): Boolean = {
    forAll(!f(_))
  }

}

// Factories
object List {

  def apply[A](elems: A*): List[A] = {
    var list: List[A] = Nil()
    for (i <- elems.length - 1 to 0 by -1) list = elems(i) :: list
    list
  }

  def of[A](elem: A, n: Int): List[A] =
    if (n == 0) Nil() else elem :: of(elem, n - 1)
}

object TryList extends App {

  import List._ // Working with the above lists
  println(List(10, 20, 30, 40) toSeq)
  val l = 10 :: 20 :: 30 :: 40 :: Nil() // same as above
  println(l.head) // 10
  println(l.tail) // 20,30,40
  println(l append l toSeq) // 10,20,30,40,10,20,30,40
  println(l append l toSeq) // as a list: 10,20,30,40,10,20,30,40
  println(l get 2) // 30
  println(of("a", 10) toSeq) // a,a,a,..,a
  println(l filter (_ <= 20) map ("a" + _) toSeq) // a10, a20
  // The code below raises a scala.NotImplementedError until you implement flatMap and zipRight
  println(l flatMap (i => i + "a" :: i + "b" :: Nil()) toSeq) // List(10a, 10b, 20a, 20b, 30a, 30b, 40a, 40b)
  println(l flatMap (i => i + 1 :: Nil()) toSeq) // List(11, 21, 31, 41)
  println(l flatMap (i => of(i, i / 10)) toSeq) // List(10, 20, 20, 30, 30, 30, 40, 40, 40, 40)
  println(l.zipRight.toSeq) // List((10,0), (20,1), (30,2), (40,3))
  println(Nil().zipRight.toSeq) // List((10,0), (20,1), (30,2), (40,3))
}
