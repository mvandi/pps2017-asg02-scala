package u05lab.code

object TicTacToe extends App {

  private val MAX_DIMENSIONS = 3

  sealed trait Player {
    def other: Player = this match {
      case X => O;
      case _ => X
    }

    override def toString: String = this match {
      case X => "X";
      case _ => "O"
    }
  }

  final case object X extends Player
  final case object O extends Player

  final case class Mark(x: Double, y: Double, player: Player)

  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Double, y: Double): Option[Player] = board
    .find { m => m.x == x && m.y == y }
    .map { _.player }

  def placeAnyMark(board: Board, player: Player): Seq[Board] = {
    def isFree(board: Board, mark: Mark): Boolean = board forall { m => m.x != mark.x || m.y != mark.y }

    Stream.range(0, MAX_DIMENSIONS)
      .flatMap(x => Stream.range(0, MAX_DIMENSIONS)
        .map(y => Mark(x, y, player)))
      .filter(isFree(board, _))
      .map(_ +: board)
  }

  def computeAnyGame(player: Player, moves: Int, checkWin: Boolean = false): Stream[Game] = {
    def computeGame(board: Board, player: Player, remainingMoves: Int): Stream[Game] = {
      def isOver(board: Board): Boolean = {
        def isOver[K](filter: Mark => Boolean)(keyExtractor: Mark => K): Boolean = board.toStream
          .filter(filter)
          .groupBy(keyExtractor)
          .exists(_._2.size == 3)

        val alwaysTrue: Mark => Boolean = _ => true
        isOver(alwaysTrue)(m => (m.x, m.player)) ||    // checks if any row has a winning combination
          isOver(alwaysTrue)(m => (m.y, m.player)) ||  // checks if any column has a winning combination
          isOver(m => m.x == m.y)(_.player) ||         // checks if the diagonal has a winning combination
          isOver(m => m.x + m.y == 2)(_.player)        // checks if the anti-diagonal has a winning combination
      }

      remainingMoves match {
        case 0 | _ if checkWin && isOver(board) => Stream(List(board))
        case _ => placeAnyMark(board, player).toStream
          .flatMap(computeGame(_, player.other, remainingMoves - 1).map(_ :+ board))
      }
    }

    computeGame(List(), player, moves)
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ".")
      if (x == 2) {
        print(" ")
        if (board == game.head) println()
      }
    }

  println("Exercise 1")

  // Exercise 1: implement find such that..
  println(find(List(Mark(0, 0, X)), 0, 0)) // Some(X)
  println(find(List(Mark(0, 0, X), Mark(0, 1, O), Mark(0, 2, X)), 0, 1)) // Some(O)
  println(find(List(Mark(0, 0, X), Mark(0, 1, O), Mark(0, 2, X)), 1, 1)) // None

  println()
  println("Exercise 2")

  // Exercise 2: implement placeAnyMark such that..
  printBoards(placeAnyMark(List(), X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  printBoards(placeAnyMark(List(Mark(0, 0, O)), X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  println()
  println("Exercise 3")

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  computeAnyGame(O, 4) foreach { g => printBoards(g); println() }
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!
  computeAnyGame(O, 9, checkWin = true) foreach { g => printBoards(g); println() }

}
