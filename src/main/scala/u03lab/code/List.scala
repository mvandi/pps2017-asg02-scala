package u03lab.code

import Option._

sealed trait List[E] {

  import List._

  override def toString: String = {
    def _toString(l: List[E]): String = l match {
      case Cons(h, t) => s", $h${_toString(t)}"
      case _ => ")"
    }

    this match {
      case Cons(h, t) => s"List($h${_toString(t)})"
      case _ => "List()"
    }
  }

}

object List {

  case class Cons[A](head: A, tail: List[A]) extends List[A]

  case class Nil[A]() extends List[A]

  def apply[E](elements: E*): List[E] = {
    elements.map(Cons(_, Nil()))
      .reduce((l1: List[E], l2: List[E]) => append(l1, l2))
  }

  def isEmpty[E](l: List[E]): Boolean = l == Nil()

  def length[E](l: List[E]): Int = l match {
    case Cons(h, t) => 1 + length(t)
    case _ => 0
  }

  def add[A](l: List[A], a: A): List[A] = l match {
    case Cons(h, t) => Cons(h, add(t, a))
    case _ => Cons(a, Nil())
  }

  // A <: C: denota che A e' un sotto-tipo di C
  // A >: C: denota che A e' un super-tipo di C
  def append[A <: C, B <: C, C](l1: List[A], l2: List[B]): List[C] = (l1, l2) match {
    case (Cons(h, t), xs) => Cons[C](h, append(t, xs))
    case (xs, Cons(h, t)) => Cons[C](h, append(xs, t))
    case _ => Nil()
  }

  def filter[A](l: List[A])(f: A => Boolean): List[A] = l match {
    case Cons(h, t) if f(h) => Cons(h, filter(t)(f))
    case Cons(h, t) if !f(h) => filter(t)(f)
    case Nil() => Nil()
  }

  def map[A, B](l: List[A])(mapper: A => B): List[B] = l match {
    case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
    case Nil() => Nil()
  }

  def flatMap[A, B](l: List[A])(mapper: A => List[B]): List[B] = l match {
    case Cons(h, t) => append(mapper(h), flatMap(t)(mapper))
    case Nil() => Nil()
  }

  @annotation.tailrec
  def drop[A](l: List[A], n: Int): List[A] = (l, n) match {
    case (Cons(h, t), x) if x > 0 => drop(t, x - 1)
    case (_, x) if x == 0 => l
    case (Nil(), _) => Nil()
  }

  @annotation.tailrec
  def dropWhile[A](l: List[A])(filter: A => Boolean): List[A] = l match {
    case Cons(h, t) if filter(h) => dropWhile(t)(filter)
    case Cons(h, t) if !filter(h) => Cons(h, t)
    case Nil() => Nil()
  }

  def dropUntil[A](l: List[A])(filter: A => Boolean): List[A] = dropWhile(l)(!filter(_))

  def take[A](l: List[A], n: Int): List[A] = (l, n) match {
    case (Cons(h, t), x) if x > 0 => Cons(h, take(t, x - 1))
    case (_, x) if x == 0 => l
    case _ => l
  }

  def takeWhile[A](l: List[A])(filter: A => Boolean): List[A] = l match {
    case Cons(h, t) if filter(h) => Cons(h, takeWhile(t)(filter))
    case _ => l
  }

  def takeUntil[A](l: List[A])(filter: A => Boolean): List[A] = takeWhile(l)(!filter(_))

  def reduce[A](l: List[A])(identity: A)(combiner: (A, A) => A): A = l match {
    case Cons(h, t) => combiner(h, reduce(t)(identity)(combiner))
    case _ => identity
  }

  def sum(l: List[Int]): Int = reduce(l)(0) {
    _ + _
  }

  def max[A](l: List[A])(comparator: (A, A) => Int): Option[A] = {
    @annotation.tailrec
    def _max(l: List[A], max: Option[A]): Option[A] = (l, max) match {
      case (Cons(h, t), None()) => _max(t, Some(h))
      case (Cons(h, t), Some(v)) if comparator(h, v) >= 0 => _max(t, Some(h))
      case (Cons(h, t), Some(v)) if comparator(h, v) < 0 => _max(t, max)
      case (Nil(), _) => max
    }

    _max(l, None())
  }

  @annotation.tailrec
  def foldLeft[A](l: List[A])(identity: A)(combiner: (A, A) => A): A = l match {
    case Cons(h, t) => foldLeft(t)(combiner(identity, h))(combiner)
    case _ => identity
  }

  @annotation.tailrec
  def foldRight[A](l: List[A])(identity: A)(combiner: (A, A) => A): A = l match {
    case Cons(h, t) => foldRight(t)(combiner(identity, h))(combiner)
    case _ => identity
  }

}

object ListTest extends App {

  import List._

  println(drop(List(10, 20, 30), 2)) // List(30)
  println(drop(List(10, 20, 30), 5)) // List()

  println(map(List(10, 20))(_ + 1)) // List(11, 21)
  println(map(List(10, 20))(":" + _ + ":")) // List(:10:, :20:)

  println(filter(List(10, 20))(_ > 15)) // List(20)
  println(filter(Cons("a", Cons("bb", Cons("ccc", Nil()))))(_.length <= 2)) // List(a, bb)

  val lst = List(3, 7, 1, 5)
  println(foldLeft(lst)(0)(_ + _)) // 16
  println(foldRight(map(lst) {
    _ toString
  })("")(_ + _)) // "3715"

}
