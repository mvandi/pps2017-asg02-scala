package u03lab.code

sealed trait Option[A]

object Option {

  case class None[A]() extends Option[A]

  case class Some[A](a: A) extends Option[A]

  def isEmpty[A](opt: Option[A]): Boolean = opt != None()

  def or[A](opt1: Option[A], opt2: Option[A]): Option[A] = opt1 match {
    case Some(_) => opt1
    case _ => opt2
  }

  def getOrElse[A, B >: A](opt: Option[A], orElse: B): B = opt match {
    case Some(a) => a
    case _ => orElse
  }

  def filter[A](opt: Option[A])(filter: A => Boolean): Option[A] = opt match {
    case Some(v) if filter(v) => Some(v)
    case _ => None()
  }

  def map[A, B](opt: Option[A])(mapper: A => B): Option[B] = opt match {
    case Some(v) => Some(mapper(v))
    case _ => None()
  }

  def map2[A <: C, B <: C, C](opt1: Option[A], opt2: Option[B])(combiner: (A, B) => C): Option[C] = (opt1, opt2) match {
    case (Some(a), Some(b)) => Some(combiner(a, b))
    case _ => None()
  }

  def ifPresent[A](opt: Option[A])(action: A => Unit): Unit = opt match {
    case Some(v) => action(v)
    case _ =>
  }

  def ifPresentOrElse[A](opt: Option[A])(presentAction: A => Unit, absentAction: () => Unit): Unit = opt match {
    case Some(v) => presentAction(v)
    case _ => absentAction()
  }

}

object OptionTest extends App {

  import Option._

  println(filter(Some(5))(_ > 2)) // Some(5)
  println(filter(Some(5))(_ > 8)) // None

  println(map(Some(5))(_ > 2)) // Some(true)
  println(map(None[Int]())(_ > 2)) // None

  println(map2(Some(5), None[Int]())(_ + _)) // None
  println(map2(None[Int](), Some(5))(_ + _)) // None
  println(map2(Some(5), Some(5))(_ + _)) // Some(10)

}
