package u03lab.code

import List._
import Person._

object Task2 extends App {

  println(drop(Cons(10, Cons(20, Cons(30, Nil()))), 2)) // Cons(30, Nil())
  println(drop(Cons(10, Cons(20, Cons(30, Nil()))), 5)) // Nil()

  println(map(Cons(10, Cons(20, Nil())))(_ + 1)) // Cons(11, Cons(21, Nil()))
  println(map(Cons(10, Cons(20, Nil())))(":" + _ + ":")) // Cons(":10:", Cons(":20:", Nil()))

  println(filter(Cons(10, Cons(20, Nil())))(_ > 15)) // Cons(20, Nil())
  println(filter(Cons("a", Cons("bb", Cons("ccc", Nil()))))(_.length <= 2)) // Cons("a", Cons("bb", Nil()))

  val comparator = (x: Int, y: Int) => x - y
  println(max(Cons(10, Cons(25, Cons(20, Nil()))))(comparator)) // Some(25)
  println(max(Nil[Int]())(comparator)) // None()

  def getCourse(t: Teacher): String = t match {
    case Teacher(_, course) => course
  }

  def getCourses(l: List[Person]): List[String] = l match {
    case Cons(h, t) => h match {
      case Teacher(_, course) => Cons(course, getCourses(t))
      case _ => getCourses(t)
    }
    case _ => Nil()
  }

  def getCourses2(l: List[Person]): List[String] = filter(map(l) {
    case Teacher(_, c) => c
    case _ => ""
  }) {
    !_.isEmpty
  }

  println(getCourses(Cons(Teacher("mirko", "PPS"), Cons(Student("mario", 2016), Nil()))))
  println(getCourses2(Cons(Teacher("mirko", "PPS"), Cons(Student("mario", 2016), Nil()))))

  println(add(Cons(1, Nil()), 2))

}
