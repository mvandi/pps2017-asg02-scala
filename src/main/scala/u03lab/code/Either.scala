package u03lab.code

import scala.util.Random

sealed trait Either[L, R]

object Either {

  import Option._

  case class Left[L, R](left: L) extends Either[L, R]

  case class Right[L, R](right: R) extends Either[L, R]

  def getLeft[L, R](e: Either[L, R]): Option[L] = e match {
    case Left(l) => Some(l)
    case _ => None()
  }

  def getLeftOrElse[L, R](e: Either[L, R], defaultLeft: L): L = e match {
    case Left(l) => l
    case _ => defaultLeft
  }

  def getRight[L, R](e: Either[L, R]): Option[R] = e match {
    case Right(r) => Some(r)
    case _ => None()
  }

  def getRightOrElse[L, R](e: Either[L, R], defaultRight: R): R = e match {
    case Right(r) => r
    case _ => defaultRight
  }

  def mapRight[L, R, R1](e: Either[L, R])(mapper: R => R1): Either[L, R1] = e match {
    case Right(r) => Right(mapper(r))
    case Left(l) => Left(l)
  }

  def mapLeft[L, R, L1](e: Either[L, R])(mapper: L => L1): Either[L1, R] = e match {
    case Left(l) => Left(mapper(l))
    case Right(r) => Right(r)
  }

  def peekRight[L, R](e: Either[L, R])(action: R => Unit): Either[L, R] = {
    e match {
      case Right(r) => action(r)
      case _ =>
    }
    e
  }

  def peekLeft[L, R](e: Either[L, R])(action: L => Unit): Either[L, R] = {
    e match {
      case Left(l) => action(l)
      case _ =>
    }
    e
  }

  def swap[L, R](e: Either[L, R]): Either[R, L] = e match {
    case Left(l) => Right(l)
    case Right(r) => Left(r)
  }

}

object EitherTest extends App {

  import Either._

  case class Task(description: String, done: Boolean)

  def getTasks: Either[String, List[Task]] = {
    if (Random.nextInt >= 0.5)
      Left("Server is down")
    else
      Right(List(Task("Drink coffee", done = true), Task("Learn Scala", done = false)))
  }

  1 to 10 foreach { _ => peekRight(peekLeft(getTasks) {
      println
    }) {
      println
    }
  }

}
