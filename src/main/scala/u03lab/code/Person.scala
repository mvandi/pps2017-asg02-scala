package u03lab.code

sealed trait Person

object Person {

  // Java's static method equivalent
  def name(p: Person): String = p match {
    case Student(n, _) => n
    case Teacher(n, _) => n
  }

  // Java's static class
  case class Student(name: String, year: Int) extends Person

  case class Teacher(name: String, course: String) extends Person

}
