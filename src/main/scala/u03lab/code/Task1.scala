package u03lab.code

object Task1 extends App {

  def compose(f: Int => Int, g: Int => Int): Int => Int = x => f(g(x))

  println(compose(_ + 1, _ * 2)(5)) // 11

  def fib(n: Int): Int = n match {
    case 0 => 0
    case 1 | 2 => 1
    case n => fib(n - 1) + fib(n - 2)
  }

  println(fib(0), fib(1), fib(2), fib(3), fib(4), fib(5)) // (0,1,1,2)

  sealed trait Person

  case class Student(name: String, year: Int) extends Person

  case class Teacher(name: String, course: String) extends Person

  implicit def personToString(p: Person): String = p match {
    case Student(name, year) => s"$name: $year"
    case Teacher(name, course) => s"$name [$course]"
  }

  println(Teacher("mirko", "PPS")) // "mirko: [PPS]"
  println(Student("mario", 2016)) // "mario: 2016"

}
